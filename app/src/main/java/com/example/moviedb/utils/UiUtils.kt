package com.example.moviedb.utils

import android.app.Activity
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import androidx.core.app.ShareCompat
import com.bumptech.glide.Glide
import com.example.moviedb.R
import com.google.android.material.snackbar.Snackbar

fun View.showSnackbar(message: String) {
    Snackbar.make(
        this,
        context.getString(R.string.cannot_fetch_movies, message),
        Snackbar.LENGTH_LONG
    ).show()
}

fun ImageView.loadImage(path: String, placeholder: Drawable? = null, error: Drawable? = null) {
    Glide.with(context)
        .load(path)
        .centerCrop()
        .placeholder(placeholder)
        .error(error)
        .into(this)
}

fun Activity.share(chooserTitle: String, text: String) {
    val intent = ShareCompat.IntentBuilder.from(this)
        .setType("text/plain")
        .setChooserTitle(chooserTitle)
        .setText(text)
        .createChooserIntent()
    
    if (intent.resolveActivity(packageManager) != null) {
        startActivity(intent)
    }
}