package com.example.moviedb.di

import com.example.moviedb.core.di.CoreModule
import com.example.moviedb.data.di.DataModule
import com.example.moviedb.domain.di.DomainModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        CoreModule::class,
        DataModule::class,
        DomainModule::class,
        AppModule::class,
        ViewModelModule::class
    ]
)
interface AppComponent : AppGraph