package com.example.moviedb.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.moviedb.domain.interactor.*
import com.example.moviedb.presentation.movies.favourites.FavouriteMoviesViewModel
import com.example.moviedb.presentation.movies.ongoing.OngoingMoviesViewModel
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Provider
import javax.inject.Singleton

@Module
class ViewModelModule {

    @Provides
    @IntoMap
    @ViewModelKey(OngoingMoviesViewModel::class)
    fun bindOngoingMoviesViewModel(
        fetchOngoingMoviesForTwoWeeksUseCase: FetchOngoingMoviesForTwoWeeksUseCase,
        getOngoingUserMoviesForTwoWeeksUseCase: GetOngoingUserMoviesForTwoWeeksUseCase,
        toggleFavouriteUseCase: ToggleFavouriteUseCase
    ): ViewModel = OngoingMoviesViewModel(
        fetchOngoingMoviesForTwoWeeksUseCase,
        getOngoingUserMoviesForTwoWeeksUseCase,
        toggleFavouriteUseCase
    )

    @Provides
    @IntoMap
    @ViewModelKey(FavouriteMoviesViewModel::class)
    fun bindFavouriteMoviesViewModel(
        removeMovieFromFavouritesUseCase: RemoveMovieFromFavouritesUseCase,
        getFavouriteUserMoviesUseCase: GetFavouriteUserMoviesUseCase
    ): ViewModel =
        FavouriteMoviesViewModel(removeMovieFromFavouritesUseCase, getFavouriteUserMoviesUseCase)

    @Provides
    @Singleton
    fun bindViewModelFactory(
        creators: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>
    ): ViewModelProvider.Factory = AppViewModelFactory(creators)
}