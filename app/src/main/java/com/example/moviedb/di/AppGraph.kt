package com.example.moviedb.di

import com.example.moviedb.App
import com.example.moviedb.core.di.CoreGraph
import com.example.moviedb.data.di.DataGraph
import com.example.moviedb.domain.di.DomainGraph
import com.example.moviedb.presentation.main.MainActivity
import com.example.moviedb.presentation.movies.MoviesContainerFragment
import com.example.moviedb.presentation.movies.favourites.FavouriteMoviesFragment
import com.example.moviedb.presentation.movies.ongoing.OngoingMoviesFragment

interface AppGraph : CoreGraph, DataGraph, DomainGraph {

    fun inject(app: App)

    fun inject(mainActivity: MainActivity)

    fun inject(moviesContainerFragment: MoviesContainerFragment)
    fun inject(ongoingMoviesFragment: OngoingMoviesFragment)
    fun inject(favouriteMoviesFragment: FavouriteMoviesFragment)
}