package com.example.moviedb.presentation.movies.favourites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.moviedb.App
import com.example.moviedb.R
import com.example.moviedb.databinding.FragmentFavouritesMoviesBinding
import com.example.moviedb.presentation.common.Fail
import com.example.moviedb.presentation.common.Success
import com.example.moviedb.presentation.movies.MoviesAdapter
import com.example.moviedb.utils.share
import com.example.moviedb.utils.showSnackbar
import javax.inject.Inject

class FavouriteMoviesFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: FavouriteMoviesViewModel by viewModels(
        factoryProducer = { viewModelFactory }
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.inst.graph.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentFavouritesMoviesBinding.inflate(
            inflater,
            container,
            false
        )

        initViews(binding)

        return binding.root
    }

    private fun initViews(binding: FragmentFavouritesMoviesBinding) {
        val moviesAdapter = MoviesAdapter(viewModel::removeFavourite) { movie ->
            activity?.share(
                getString(R.string.share_link_to_favourite_movie),
                getString(R.string.movie_info, movie.title, movie.overview)
            )
        }

        binding.rvMovies.adapter = moviesAdapter

        viewModel.favouriteUserMovies.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Success -> {
                    binding.tvEmptyState.visibility =
                        if (it.data.isEmpty()) View.VISIBLE else View.GONE
                    binding.rvMovies.visibility = if (it.data.isEmpty()) View.GONE else View.VISIBLE
                    moviesAdapter.submitList(it.data)
                }
                is Fail -> {
                    binding.root.showSnackbar(it.reason())
                }
            }
        })

        viewModel.removedFromFavouriteState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Fail -> binding.root.showSnackbar(it.reason())
            }
        })
    }
}