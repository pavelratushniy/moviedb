package com.example.moviedb.presentation.movies

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.moviedb.App
import com.example.moviedb.R
import com.example.moviedb.databinding.FragmentMoviesContainerBinding
import com.example.moviedb.presentation.movies.favourites.FavouriteMoviesFragment
import com.example.moviedb.presentation.movies.ongoing.OngoingMoviesFragment
import com.google.android.material.tabs.TabLayoutMediator
import javax.inject.Inject

class MoviesContainerFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.inst.graph.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentMoviesContainerBinding.inflate(
            inflater,
            container,
            false
        )

        initViews(binding)

        return binding.root
    }

    private fun initViews(binding: FragmentMoviesContainerBinding) {
        binding.pagerMovies.apply {
            adapter = MoviesTabsAdapter(this@MoviesContainerFragment)
            offscreenPageLimit = Tab.values().size
        }

        TabLayoutMediator(binding.tabLayout, binding.pagerMovies) { tab, position ->
            tab.text = getString(
                when (position) {
                    Tab.ONGOING.ordinal -> R.string.films
                    Tab.FAVOURITES.ordinal -> R.string.favourites
                    else -> throw IllegalStateException("Unknown movies tab at position $position")
                }
            )
        }.attach()
    }
}

private enum class Tab {
    ONGOING,
    FAVOURITES
}

private class MoviesTabsAdapter(
    moviesContainerFragment: MoviesContainerFragment
) : FragmentStateAdapter(moviesContainerFragment) {

    override fun getItemCount(): Int {
        return Tab.values().size
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            Tab.ONGOING.ordinal -> OngoingMoviesFragment()
            Tab.FAVOURITES.ordinal -> FavouriteMoviesFragment()
            else -> throw IllegalArgumentException("Unknown movies container pager position")
        }
    }
}
