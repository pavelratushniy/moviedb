package com.example.moviedb.presentation.movies.favourites

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.moviedb.core.utils.logDebug
import com.example.moviedb.core.utils.logError
import com.example.moviedb.core.vo.Movie
import com.example.moviedb.core.vo.UserMovie
import com.example.moviedb.domain.interactor.GetFavouriteUserMoviesUseCase
import com.example.moviedb.domain.interactor.RemoveMovieFromFavouritesUseCase
import com.example.moviedb.presentation.common.*

class FavouriteMoviesViewModel(
    private val removeMovieFromFavouritesUseCase: RemoveMovieFromFavouritesUseCase,
    private val getFavouriteUserMoviesUseCase: GetFavouriteUserMoviesUseCase
) : BaseViewModel() {

    private val _favouriteUserMovies = MutableLiveData<State<List<UserMovie>>>()
    private val _removedFromFavouriteState = SingleLiveEvent<State<Unit>>()

    val favouriteUserMovies: LiveData<State<List<UserMovie>>> by lazy {
        return@lazy _favouriteUserMovies.apply {
            subscribeForFavouriteUserMovies()
        }
    }
    val removedFromFavouriteState: LiveData<State<Unit>> = _removedFromFavouriteState

    fun removeFavourite(movie: Movie) {
        if (_removedFromFavouriteState.value == Loading) {
            return
        }

        removeMovieFromFavouritesUseCase(movie)
            .doOnSubscribe { _removedFromFavouriteState.value = Loading }
            .subscribe({
                logDebug(message = "Successfully removed favourite")
                _removedFromFavouriteState.postValue(Success(Unit))
            }, {
                logError(message = "Cannot remove favourite", throwable = it)
                _removedFromFavouriteState.postValue(Fail(throwable = it))
            }).disposeOnClear()
    }

    private fun subscribeForFavouriteUserMovies() {
        getFavouriteUserMoviesUseCase()
            .doOnSubscribe { _favouriteUserMovies.value = Loading }
            .subscribe({
                logDebug(message = "Successfully got favourite user movies: $it")
                _favouriteUserMovies.postValue(Success(it))
            }, {
                logError(message = "Cannot get favourite user movies", throwable = it)
            }).disposeOnClear()
    }
}