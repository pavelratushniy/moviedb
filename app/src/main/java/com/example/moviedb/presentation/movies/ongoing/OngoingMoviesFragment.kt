package com.example.moviedb.presentation.movies.ongoing

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.moviedb.App
import com.example.moviedb.R
import com.example.moviedb.core.vo.UserMovie
import com.example.moviedb.databinding.FragmentOngoingMoviesBinding
import com.example.moviedb.presentation.common.Fail
import com.example.moviedb.presentation.common.Loading
import com.example.moviedb.presentation.common.State
import com.example.moviedb.presentation.common.Success
import com.example.moviedb.presentation.movies.MoviesAdapter
import com.example.moviedb.utils.share
import com.example.moviedb.utils.showSnackbar
import javax.inject.Inject

class OngoingMoviesFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: OngoingMoviesViewModel by viewModels(
        factoryProducer = { viewModelFactory }
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.inst.graph.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentOngoingMoviesBinding.inflate(
            inflater,
            container,
            false
        )

        initViews(binding)

        return binding.root
    }

    private fun initViews(binding: FragmentOngoingMoviesBinding) {
        binding.swipeRefreshLayoutMovies.setOnRefreshListener {
            viewModel.refresh()
        }

        val moviesAdapter = MoviesAdapter(viewModel::toggleFavourite) { movie ->
            activity?.share(
                getString(R.string.share_link_to_ongoing_movie),
                getString(R.string.movie_info, movie.title, movie.overview)
            )
        }
        binding.rvMovies.adapter = moviesAdapter

        viewModel.userMovies.observe(viewLifecycleOwner, Observer {
            bindUserMoviesState(it, binding, moviesAdapter)
        })

        viewModel.toggleFavouriteState.observe(viewLifecycleOwner, Observer {
            bindToggleFavouriteState(it, binding)
        })
    }

    private fun bindUserMoviesState(
        userMoviesState: State<List<UserMovie>>,
        binding: FragmentOngoingMoviesBinding,
        moviesAdapter: MoviesAdapter
    ) {
        binding.swipeRefreshLayoutMovies.isRefreshing = userMoviesState == Loading
        when (userMoviesState) {
            is Loading -> {
                binding.tvEmptyState.visibility = View.GONE
                binding.rvMovies.visibility = View.GONE
            }
            is Success -> {
                binding.tvEmptyState.visibility =
                    if (userMoviesState.data.isEmpty()) View.VISIBLE else View.GONE
                binding.rvMovies.visibility =
                    if (userMoviesState.data.isEmpty()) View.GONE else View.VISIBLE
                moviesAdapter.submitList(userMoviesState.data)
            }
            is Fail -> {
                binding.root.showSnackbar(userMoviesState.reason())
            }
        }
    }

    private fun bindToggleFavouriteState(
        toggleFavouriteState: State<Unit>,
        binding: FragmentOngoingMoviesBinding
    ) {
        when (toggleFavouriteState) {
            is Fail -> binding.root.showSnackbar(toggleFavouriteState.reason())
        }
    }
}