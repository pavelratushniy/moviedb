package com.example.moviedb.presentation.movies

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.moviedb.R
import com.example.moviedb.core.vo.Movie
import com.example.moviedb.core.vo.UserMovie
import com.example.moviedb.databinding.ItemMovieBinding
import com.example.moviedb.utils.loadImage

class MoviesAdapter(
    private val toggleFavouriteAction: (Movie) -> Unit,
    private val shareAction: (Movie) -> Unit
) : ListAdapter<UserMovie, MoviesViewHolder>(MoviesDiffUtilItemCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {
        return MoviesViewHolder(
            ItemMovieBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            toggleFavouriteAction,
            shareAction
        )
    }

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        return holder.bind(getItem(position))
    }

    override fun onBindViewHolder(
        holder: MoviesViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
        } else {
            val userMovie = getItem(position)
            payloads.forEach {
                when (it) {
                    is MoviesDiffUtilItemCallback.MoviePayload -> {
                        holder.bind(userMovie, it)
                    }
                }
            }
        }
    }
}

class MoviesViewHolder(
    private val binding: ItemMovieBinding,
    private val toggleFavouriteAction: (Movie) -> Unit,
    private val shareAction: (Movie) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(
        userMovie: UserMovie,
        payload: MoviesDiffUtilItemCallback.MoviePayload
    ) {
        if (payload.titleChanged) bindTitle(userMovie.movie)
        if (payload.overviewChanged) bindOverview(userMovie.movie)
        if (payload.posterPathChanged) bindPoster(userMovie.movie)
        if (payload.favouriteChanged) bindFavourite(userMovie.favourite)
    }

    fun bind(
        userMovie: UserMovie
    ) {
        binding.btnFavourite.setOnClickListener {
            toggleFavouriteAction(userMovie.movie)
        }

        binding.btnShare.setOnClickListener {
            shareAction(userMovie.movie)
        }

        bindTitle(userMovie.movie)
        bindOverview(userMovie.movie)
        bindPoster(userMovie.movie)
        bindFavourite(userMovie.favourite)
    }

    private fun bindTitle(movie: Movie) {
        binding.tvTitle.text = movie.title
    }

    private fun bindOverview(movie: Movie) {
        binding.tvDescription.text = movie.overview
    }

    private fun bindPoster(movie: Movie) {
        val posterPath = movie.posterPath
        val posterPlaceholder = binding.root.context.getDrawable(R.drawable.ic_movie_black)
        if (posterPath == null) {
            binding.ivPoster.setImageDrawable(posterPlaceholder)
        } else {
            binding.ivPoster.loadImage(
                posterPath,
                posterPlaceholder,
                posterPlaceholder
            )
        }
    }

    private fun bindFavourite(favourite: Boolean) {
        binding.btnFavourite.setText(
            if (favourite) R.string.remove_from_favourites else R.string.add_to_favourites
        )
    }
}

class MoviesDiffUtilItemCallback : DiffUtil.ItemCallback<UserMovie>() {

    override fun areItemsTheSame(oldItem: UserMovie, newItem: UserMovie): Boolean {
        return oldItem.movie.movieId == newItem.movie.movieId
    }

    override fun areContentsTheSame(oldItem: UserMovie, newItem: UserMovie): Boolean {
        return oldItem == newItem
    }

    override fun getChangePayload(oldItem: UserMovie, newItem: UserMovie): Any? {
        return MoviePayload(
            oldItem.movie.title != newItem.movie.title,
            oldItem.movie.overview != newItem.movie.overview,
            oldItem.movie.posterPath != newItem.movie.posterPath,
            oldItem.favourite != newItem.favourite
        )
    }

    class MoviePayload(
        val titleChanged: Boolean,
        val overviewChanged: Boolean,
        val posterPathChanged: Boolean,
        val favouriteChanged: Boolean
    )
}