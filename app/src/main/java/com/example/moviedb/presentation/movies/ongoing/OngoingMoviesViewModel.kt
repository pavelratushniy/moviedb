package com.example.moviedb.presentation.movies.ongoing

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.moviedb.core.utils.logDebug
import com.example.moviedb.core.utils.logError
import com.example.moviedb.core.vo.Movie
import com.example.moviedb.core.vo.UserMovie
import com.example.moviedb.domain.interactor.FetchOngoingMoviesForTwoWeeksUseCase
import com.example.moviedb.domain.interactor.GetOngoingUserMoviesForTwoWeeksUseCase
import com.example.moviedb.domain.interactor.ToggleFavouriteUseCase
import com.example.moviedb.presentation.common.*
import io.reactivex.disposables.Disposable

class OngoingMoviesViewModel(
    private val fetchOngoingMoviesForTwoWeeksUseCase: FetchOngoingMoviesForTwoWeeksUseCase,
    private val getOngoingUserMoviesForTwoWeeksUseCase: GetOngoingUserMoviesForTwoWeeksUseCase,
    private val toggleFavouriteUseCase: ToggleFavouriteUseCase
) : BaseViewModel() {

    private var getMoviesDisposable: Disposable? = null

    private val _userMovies = MutableLiveData<State<List<UserMovie>>>()
    private val _toggleFavouriteState = SingleLiveEvent<State<Unit>>()

    val userMovies: LiveData<State<List<UserMovie>>> by lazy {
        return@lazy _userMovies.apply {
            refresh()
        }
    }
    val toggleFavouriteState: LiveData<State<Unit>> = _toggleFavouriteState

    fun refresh() {
        getMoviesDisposable?.dispose()

        getMoviesDisposable = fetchOngoingMoviesForTwoWeeksUseCase()
            .ignoreElement()
            .doOnError {
                logError(message = "Cannot fetch ongoing movies for two weeks", throwable = it)
                _userMovies.postValue(Fail(throwable = it))
            }
            .onErrorComplete()
            .andThen(getOngoingUserMoviesForTwoWeeksUseCase())
            .doOnSubscribe { _userMovies.value = Loading }
            .subscribe({
                logDebug(message = "Successfully got ongoing user movies for two weeks: $it")
                _userMovies.postValue(Success(it))
            }, {
                logError(message = "Cannot get ongoing user movies for two weeks", throwable = it)
                _userMovies.postValue(Fail(throwable = it))
            })
    }

    fun toggleFavourite(movie: Movie) {
        if (_toggleFavouriteState.value == Loading) {
            return
        }

        toggleFavouriteUseCase(movie)
            .doOnSubscribe { _toggleFavouriteState.value = Loading }
            .subscribe({
                logDebug(message = "Successfully toggled favourite")
                _toggleFavouriteState.postValue(Success(Unit))
            }, {
                logError(message = "Cannot toggle favourite", throwable = it)
                _toggleFavouriteState.postValue(Fail(throwable = it))
            }).disposeOnClear()
    }
}