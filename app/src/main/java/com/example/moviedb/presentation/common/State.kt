package com.example.moviedb.presentation.common

sealed class State<out T : Any>

object Loading : State<Nothing>()

data class Success<out T : Any>(val data: T) : State<T>()

data class Fail(
    val message: String = "",
    val throwable: Throwable = IllegalStateException()
) : State<Nothing>() {

    fun reason(): String = if (message.isEmpty()) throwable.message ?: "" else message
}
