package com.example.moviedb

import android.app.Application
import com.example.moviedb.data.di.DataModule
import com.example.moviedb.di.AppGraph
import com.example.moviedb.di.DaggerAppComponent
import net.danlew.android.joda.JodaTimeAndroid
import timber.log.Timber

class App : Application() {

    lateinit var graph: AppGraph

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())

        inst = this

        graph = createGraph()
        graph.inject(this)

        JodaTimeAndroid.init(this)
    }

    private fun createGraph(): AppGraph = DaggerAppComponent.builder()
        .dataModule(DataModule(this))
        .build()

    companion object {
        lateinit var inst: App
    }
}