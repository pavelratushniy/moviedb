package com.example.moviedb.movies.ongoing

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.moviedb.core.vo.Movie
import com.example.moviedb.core.vo.UserMovie
import com.example.moviedb.domain.interactor.FetchOngoingMoviesForTwoWeeksUseCase
import com.example.moviedb.domain.interactor.GetOngoingUserMoviesForTwoWeeksUseCase
import com.example.moviedb.domain.interactor.ToggleFavouriteUseCase
import com.example.moviedb.presentation.common.Loading
import com.example.moviedb.presentation.common.Success
import com.example.moviedb.presentation.movies.ongoing.OngoingMoviesViewModel
import com.nhaarman.mockitokotlin2.*
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.SingleSubject
import org.joda.time.LocalDate
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class OngoingMoviesViewModelUnitTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private lateinit var toggleFavouriteUseCase: ToggleFavouriteUseCase
    private lateinit var fetchOngoingMoviesForTwoWeeksUseCase: FetchOngoingMoviesForTwoWeeksUseCase
    private lateinit var getOngoingUserMoviesForTwoWeeksUseCase: GetOngoingUserMoviesForTwoWeeksUseCase

    private lateinit var ongoingMoviesViewModel: OngoingMoviesViewModel

    @Before
    fun init() {
        toggleFavouriteUseCase = mock()
        fetchOngoingMoviesForTwoWeeksUseCase = mock()
        getOngoingUserMoviesForTwoWeeksUseCase = mock()

        ongoingMoviesViewModel = OngoingMoviesViewModel(
            fetchOngoingMoviesForTwoWeeksUseCase,
            getOngoingUserMoviesForTwoWeeksUseCase,
            toggleFavouriteUseCase
        )
    }

    @Test
    fun `test fetch on subscribe`() {
        val fetchMoviesSubject = SingleSubject.create<List<Movie>>()
        whenever(fetchOngoingMoviesForTwoWeeksUseCase.invoke()) doReturn fetchMoviesSubject

        val ongoingMoviesSubject = BehaviorSubject.create<List<UserMovie>>()
        whenever(getOngoingUserMoviesForTwoWeeksUseCase.invoke()) doReturn ongoingMoviesSubject

        ongoingMoviesViewModel.userMovies

        verify(fetchOngoingMoviesForTwoWeeksUseCase, times(1)).invoke()
    }

    @Test
    fun `test fetch on refresh`() {
        val fetchMoviesSubject = SingleSubject.create<List<Movie>>()
        whenever(fetchOngoingMoviesForTwoWeeksUseCase.invoke()) doReturn fetchMoviesSubject

        val ongoingMoviesSubject = BehaviorSubject.create<List<UserMovie>>()
        whenever(getOngoingUserMoviesForTwoWeeksUseCase.invoke()) doReturn ongoingMoviesSubject

        ongoingMoviesViewModel.refresh()

        verify(fetchOngoingMoviesForTwoWeeksUseCase, times(1)).invoke()
    }

    @Test
    fun `test fetch ongoing movies for two weeks`() {
        val fetchMoviesSubject = SingleSubject.create<List<Movie>>()
        whenever(fetchOngoingMoviesForTwoWeeksUseCase.invoke()) doReturn fetchMoviesSubject

        val ongoingMoviesSubject = BehaviorSubject.create<List<UserMovie>>()
        whenever(getOngoingUserMoviesForTwoWeeksUseCase.invoke()) doReturn ongoingMoviesSubject

        assert(ongoingMoviesViewModel.userMovies.value == Loading)

        val matrixMovie = Movie(
            20,
            "The Matrix",
            "overview",
            LocalDate(2020, 1, 1),
            null
        )
        val movies = listOf(
            matrixMovie
        )
        val userMovies = listOf(
            UserMovie(matrixMovie, false)
        )
        fetchMoviesSubject.onSuccess(movies)
        ongoingMoviesSubject.onNext(userMovies)

        assert(ongoingMoviesViewModel.userMovies.value == Success(userMovies))
    }

    @Test
    fun `test toggling favourite`() {
        val toggleFavouriteSubject = CompletableSubject.create()
        whenever(toggleFavouriteUseCase.invoke(any())) doReturn toggleFavouriteSubject

        ongoingMoviesViewModel.toggleFavourite(
            Movie(
                20,
                "The Matrix",
                "overview",
                LocalDate(2020, 1, 1),
                null
            )
        )
        assert(ongoingMoviesViewModel.toggleFavouriteState.value == Loading)

        toggleFavouriteSubject.onComplete()

        assert(ongoingMoviesViewModel.toggleFavouriteState.value == Success(Unit))
    }

    @Test
    fun `test not toggling favourite twice`() {
        val toggleFavouriteSubject = CompletableSubject.create()
        whenever(toggleFavouriteUseCase.invoke(any())) doReturn toggleFavouriteSubject

        val movie = Movie(
            20,
            "The Matrix",
            "overview",
            LocalDate(2020, 1, 1),
            null
        )

        ongoingMoviesViewModel.toggleFavourite(movie)
        ongoingMoviesViewModel.toggleFavourite(movie)

        verify(toggleFavouriteUseCase, times(1)).invoke(movie)
    }
}