package com.example.moviedb.movies.ongoing

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.moviedb.core.vo.Movie
import com.example.moviedb.core.vo.UserMovie
import com.example.moviedb.domain.interactor.GetFavouriteUserMoviesUseCase
import com.example.moviedb.domain.interactor.RemoveMovieFromFavouritesUseCase
import com.example.moviedb.presentation.common.Loading
import com.example.moviedb.presentation.common.Success
import com.example.moviedb.presentation.movies.favourites.FavouriteMoviesViewModel
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.CompletableSubject
import org.joda.time.LocalDate
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class FavouriteMoviesViewModelUnitTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private lateinit var getFavouriteUserMoviesUseCase: GetFavouriteUserMoviesUseCase
    private lateinit var removeFromFavouriteUserMoviesUseCase: RemoveMovieFromFavouritesUseCase

    private lateinit var favouriteMoviesViewModel: FavouriteMoviesViewModel

    @Before
    fun init() {
        getFavouriteUserMoviesUseCase = mock()
        removeFromFavouriteUserMoviesUseCase = mock()

        favouriteMoviesViewModel = FavouriteMoviesViewModel(
            removeFromFavouriteUserMoviesUseCase,
            getFavouriteUserMoviesUseCase
        )
    }

    @Test
    fun `test getting favourites`() {
        val favouriteMoviesSubject = BehaviorSubject.create<List<UserMovie>>()
        whenever(getFavouriteUserMoviesUseCase.invoke()) doReturn favouriteMoviesSubject

        assert(favouriteMoviesViewModel.favouriteUserMovies.value == Loading)

        val movies = listOf(
            UserMovie(
                Movie(
                    20,
                    "The Matrix",
                    "overview",
                    LocalDate(2020, 1, 1),
                    null
                ), true
            )
        )
        favouriteMoviesSubject.onNext(movies)

        assert(favouriteMoviesViewModel.favouriteUserMovies.value == Success(movies))
    }

    @Test
    fun `test removing favourite`() {
        val removeFavouriteSubject = CompletableSubject.create()
        whenever(removeFromFavouriteUserMoviesUseCase.invoke(any())) doReturn removeFavouriteSubject

        val movie = Movie(
            20,
            "The Matrix",
            "overview",
            LocalDate(2020, 1, 1),
            null
        )

        favouriteMoviesViewModel.removeFavourite(movie)

        assert(favouriteMoviesViewModel.removedFromFavouriteState.value == Loading)

        removeFavouriteSubject.onComplete()

        assert(favouriteMoviesViewModel.removedFromFavouriteState.value == Success(Unit))
    }
}