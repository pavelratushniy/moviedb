package com.example.moviedb.domain.interactor

import com.example.moviedb.core.vo.FavouriteMovie
import com.example.moviedb.core.vo.Movie
import com.example.moviedb.core.vo.UserMovie
import com.example.moviedb.data.repo.favouritemovies.FavouriteMoviesRepo
import com.example.moviedb.data.repo.movies.MoviesRepo
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.joda.time.LocalDate
import org.junit.Before
import org.junit.Test

class GetAllUserMoviesUseCaseUnitTest {

    private lateinit var moviesRepo: MoviesRepo
    private lateinit var favouriteMoviesRepo: FavouriteMoviesRepo

    private lateinit var getAllUserMoviesUseCase: GetAllUserMoviesUseCase

    @Before
    fun init() {
        moviesRepo = mock()
        favouriteMoviesRepo = mock()

        getAllUserMoviesUseCase = GetAllUserMoviesUseCaseImpl(
            moviesRepo,
            favouriteMoviesRepo,
            Schedulers.trampoline()
        )
    }

    @Test
    fun `test getting all user movies`() {
        val getAllMoviesSubject = BehaviorSubject.create<List<Movie>>()
        whenever(moviesRepo.getAll()) doReturn getAllMoviesSubject

        val getAllFavouriteMoviesSubject = BehaviorSubject.create<List<FavouriteMovie>>()
        whenever(favouriteMoviesRepo.getAll()) doReturn getAllFavouriteMoviesSubject

        val observer = getAllUserMoviesUseCase().test()

        val matrixMovie = Movie(
            20,
            "The Matrix",
            "overview",
            LocalDate(2020, 1, 20),
            null
        )
        val darkKnightMovie = Movie(
            30,
            "The Dark Knight",
            "overview",
            LocalDate(2020, 1, 19),
            null
        )

        getAllMoviesSubject.onNext(listOf(matrixMovie, darkKnightMovie))
        getAllFavouriteMoviesSubject.onNext(
            listOf(
                FavouriteMovie(1, 20)
            )
        )

        observer.assertValueCount(1)
        observer.assertValue(
            listOf(
                UserMovie(matrixMovie, true),
                UserMovie(darkKnightMovie, false)
            )
        )
        observer.dispose()
    }
}