package com.example.moviedb.domain.interactor

import com.example.moviedb.core.vo.Movie
import com.example.moviedb.data.repo.movies.MoviesRepo
import com.nhaarman.mockitokotlin2.*
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.SingleSubject
import org.joda.time.DateTimeUtils
import org.joda.time.LocalDate
import org.joda.time.LocalTime
import org.junit.Before
import org.junit.Test

class FetchOngoingMoviesForTwoWeeksUseCaseUnitTest {

    private lateinit var moviesRepo: MoviesRepo

    private lateinit var fetchOngoingMoviesForTwoWeeksUseCase: FetchOngoingMoviesForTwoWeeksUseCase

    @Before
    fun init() {
        moviesRepo = mock()

        fetchOngoingMoviesForTwoWeeksUseCase = FetchOngoingMoviesForTwoWeeksUseCaseImpl(
            moviesRepo,
            Schedulers.trampoline()
        )
    }

    @Test
    fun `test fetch ongoing movies for two weeks`() {
        val fetchMoviesSubject = SingleSubject.create<List<Movie>>()
        whenever(moviesRepo.fetch(any(), any())) doReturn fetchMoviesSubject

        val to = LocalDate(2020, 1, 20)
        val from = to.minusWeeks(2)
        DateTimeUtils.setCurrentMillisFixed(
            to.toDateTime(LocalTime(0, 0, 0)).millis
        )

        val fetchObserver = fetchOngoingMoviesForTwoWeeksUseCase().test()

        val movies = listOf(
            Movie(
                20,
                "The Matrix",
                "overview",
                LocalDate(2020, 1, 1),
                null
            )
        )

        fetchMoviesSubject.onSuccess(movies)

        verify(moviesRepo, times(1)).fetch(from, to)
        fetchObserver.assertComplete()
    }
}