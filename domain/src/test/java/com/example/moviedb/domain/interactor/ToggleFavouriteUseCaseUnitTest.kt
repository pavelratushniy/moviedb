package com.example.moviedb.domain.interactor

import com.example.moviedb.core.vo.FavouriteMovie
import com.example.moviedb.core.vo.Movie
import com.example.moviedb.data.repo.favouritemovies.FavouriteMoviesRepo
import com.nhaarman.mockitokotlin2.*
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.MaybeSubject
import org.joda.time.DateTimeUtils
import org.joda.time.LocalDate
import org.junit.Before
import org.junit.Test

class ToggleFavouriteUseCaseUnitTest {

    private lateinit var addMovieToFavouritesUseCase: AddMovieToFavouritesUseCase
    private lateinit var removeMovieFromFavouritesUseCase: RemoveMovieFromFavouritesUseCase
    private lateinit var favouriteMoviesRepo: FavouriteMoviesRepo

    private lateinit var toggleFavouriteUseCase: ToggleFavouriteUseCase

    @Before
    fun init() {
        addMovieToFavouritesUseCase = mock()
        removeMovieFromFavouritesUseCase = mock()
        favouriteMoviesRepo = mock()

        toggleFavouriteUseCase = ToggleFavouriteUseCaseImpl(
            addMovieToFavouritesUseCase,
            removeMovieFromFavouritesUseCase,
            favouriteMoviesRepo,
            Schedulers.trampoline()
        )
    }

    @Test
    fun `test toggling favourite - delete from favourites`() {
        val removeFavouriteMovieSubject = CompletableSubject.create()
        whenever(removeMovieFromFavouritesUseCase(any())) doReturn removeFavouriteMovieSubject

        val getFavouriteMovieByMovieIdSubject = MaybeSubject.create<FavouriteMovie>()
        whenever(favouriteMoviesRepo.getByMovieId(any())) doReturn getFavouriteMovieByMovieIdSubject

        val movie = Movie(
            20,
            "The Matrix",
            "overview",
            LocalDate(2020, 1, 20),
            null
        )

        val observer = toggleFavouriteUseCase(movie).test()

        getFavouriteMovieByMovieIdSubject.onSuccess(FavouriteMovie(1, movie.movieId))

        verify(removeMovieFromFavouritesUseCase, times(1)).invoke(movie)
        removeFavouriteMovieSubject.onComplete()

        observer.assertComplete()
        observer.dispose()
    }

    @Test
    fun `test toggling favourite - add to favourites`() {
        val getFavouriteMovieByMovieIdSubject = MaybeSubject.create<FavouriteMovie>()
        whenever(favouriteMoviesRepo.getByMovieId(any())) doReturn getFavouriteMovieByMovieIdSubject

        val saveFavouriteMovieSubject = CompletableSubject.create()
        whenever(addMovieToFavouritesUseCase(any())) doReturn saveFavouriteMovieSubject

        DateTimeUtils.setCurrentMillisFixed(1)

        val movie = Movie(
            20,
            "The Matrix",
            "overview",
            LocalDate(2020, 1, 20),
            null
        )

        val observer = toggleFavouriteUseCase(movie).test()

        getFavouriteMovieByMovieIdSubject.onComplete()

        verify(addMovieToFavouritesUseCase, times(1)).invoke(movie)
        saveFavouriteMovieSubject.onComplete()

        observer.assertComplete()
        observer.dispose()
    }
}