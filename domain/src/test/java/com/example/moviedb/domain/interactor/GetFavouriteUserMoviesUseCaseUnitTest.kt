package com.example.moviedb.domain.interactor

import com.example.moviedb.core.vo.FavouriteMovie
import com.example.moviedb.core.vo.Movie
import com.example.moviedb.core.vo.UserMovie
import com.example.moviedb.data.repo.favouritemovies.FavouriteMoviesRepo
import com.example.moviedb.data.repo.movies.MoviesRepo
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.joda.time.LocalDate
import org.junit.Before
import org.junit.Test

class GetFavouriteUserMoviesUseCaseUnitTest {

    private lateinit var favouriteMoviesRepo: FavouriteMoviesRepo
    private lateinit var moviesRepo: MoviesRepo

    private lateinit var getFavouriteUserMoviesUseCase: GetFavouriteUserMoviesUseCase

    @Before
    fun init() {
        favouriteMoviesRepo = mock()
        moviesRepo = mock()

        getFavouriteUserMoviesUseCase = GetFavouriteUserMoviesUseCaseImpl(
            moviesRepo,
            favouriteMoviesRepo,
            Schedulers.trampoline()
        )
    }

    @Test
    fun `test getting favourite user movies`() {
        val getAllMoviesSubject = BehaviorSubject.create<List<Movie>>()
        whenever(moviesRepo.getAll()) doReturn getAllMoviesSubject

        val getAllFavouriteMoviesSubject = BehaviorSubject.create<List<FavouriteMovie>>()
        whenever(favouriteMoviesRepo.getAll()) doReturn getAllFavouriteMoviesSubject

        val observer = getFavouriteUserMoviesUseCase().test()

        val matrixMovie = Movie(
            20,
            "The Matrix",
            "overview",
            LocalDate(2020, 1, 20),
            null
        )
        val darkKnightMovie = Movie(
            30,
            "The Dark Knight",
            "overview",
            LocalDate(2020, 1, 19),
            null
        )

        getAllMoviesSubject.onNext(listOf(matrixMovie, darkKnightMovie))
        getAllFavouriteMoviesSubject.onNext(
            listOf(
                FavouriteMovie(1, 20),
                FavouriteMovie(2, 30)
            ).sortedByDescending { it.timestamp }
        )

        observer.assertValueCount(1)
        observer.assertValue(
            listOf(
                UserMovie(darkKnightMovie, true),
                UserMovie(matrixMovie, true)
            )
        )
        observer.dispose()
    }
}