package com.example.moviedb.domain.interactor

import com.example.moviedb.core.vo.FavouriteMovie
import com.example.moviedb.core.vo.Movie
import com.example.moviedb.data.repo.favouritemovies.FavouriteMoviesRepo
import com.nhaarman.mockitokotlin2.*
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import org.joda.time.DateTimeUtils
import org.joda.time.LocalDate
import org.junit.Before
import org.junit.Test

class AddMovieToFavouritesUseCaseUnitTest {

    private lateinit var favouriteMoviesRepo: FavouriteMoviesRepo

    private lateinit var addMovieToFavouritesUseCase: AddMovieToFavouritesUseCase

    @Before
    fun init() {
        favouriteMoviesRepo = mock()

        addMovieToFavouritesUseCase = AddMovieToFavouritesUseCaseImpl(
            favouriteMoviesRepo,
            Schedulers.trampoline()
        )
    }

    @Test
    fun `test adding to favourites`() {
        val saveFavouriteMovieSubject = CompletableSubject.create()
        whenever(favouriteMoviesRepo.save(any())) doReturn saveFavouriteMovieSubject

        DateTimeUtils.setCurrentMillisFixed(1)

        val movie = Movie(
            20,
            "The Matrix",
            "overview",
            LocalDate(2020, 1, 20),
            null
        )

        val observer = addMovieToFavouritesUseCase(movie).test()

        verify(favouriteMoviesRepo, times(1)).save(FavouriteMovie(1, movie.movieId))
        saveFavouriteMovieSubject.onComplete()

        observer.assertComplete()
        observer.dispose()
    }
}