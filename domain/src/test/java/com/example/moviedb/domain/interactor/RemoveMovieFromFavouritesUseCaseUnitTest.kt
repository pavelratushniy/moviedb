package com.example.moviedb.domain.interactor

import com.example.moviedb.core.vo.Movie
import com.example.moviedb.data.repo.favouritemovies.FavouriteMoviesRepo
import com.nhaarman.mockitokotlin2.*
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import org.joda.time.LocalDate
import org.junit.Before
import org.junit.Test

class RemoveMovieFromFavouritesUseCaseUnitTest {

    private lateinit var favouriteMoviesRepo: FavouriteMoviesRepo

    private lateinit var removeMovieFromFavouritesUseCase: RemoveMovieFromFavouritesUseCase

    @Before
    fun init() {
        favouriteMoviesRepo = mock()

        removeMovieFromFavouritesUseCase = RemoveMovieFromFavouritesUseCaseImpl(
            favouriteMoviesRepo,
            Schedulers.trampoline()
        )
    }

    @Test
    fun `test removing from favourites`() {
        val deleteFavouriteMovieSubject = CompletableSubject.create()
        whenever(favouriteMoviesRepo.deleteByMovieId(any())) doReturn deleteFavouriteMovieSubject

        val movie = Movie(
            20,
            "The Matrix",
            "overview",
            LocalDate(2020, 1, 20),
            null
        )

        val observer = removeMovieFromFavouritesUseCase(movie).test()

        verify(favouriteMoviesRepo, times(1)).deleteByMovieId(movie.movieId)
        deleteFavouriteMovieSubject.onComplete()

        observer.assertComplete()
        observer.dispose()
    }
}