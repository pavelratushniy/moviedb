package com.example.moviedb.domain.interactor

import com.example.moviedb.core.vo.Movie
import com.example.moviedb.core.vo.UserMovie
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.joda.time.DateTimeUtils
import org.joda.time.LocalDate
import org.joda.time.LocalTime
import org.junit.Before
import org.junit.Test

class GetOngoingUserMoviesForTwoWeeksUseCaseUnitTest {

    private lateinit var getAllUserMoviesUseCase: GetAllUserMoviesUseCase

    private lateinit var getOngoingUserMoviesForTwoWeeksUseCase: GetOngoingUserMoviesForTwoWeeksUseCase

    @Before
    fun init() {
        getAllUserMoviesUseCase = mock()

        getOngoingUserMoviesForTwoWeeksUseCase = GetOngoingUserMoviesForTwoWeeksUseCaseImpl(
            getAllUserMoviesUseCase,
            Schedulers.trampoline()
        )
    }

    @Test
    fun `test getting ongoing user movies for two weeks - ignore old`() {
        val getAllUserMoviesSubject = BehaviorSubject.create<List<UserMovie>>()
        whenever(getAllUserMoviesUseCase()) doReturn getAllUserMoviesSubject

        DateTimeUtils.setCurrentMillisFixed(
            LocalDate(2020, 1, 28)
                .toDateTime(LocalTime(0, 0, 0)).millis
        )

        val observer = getOngoingUserMoviesForTwoWeeksUseCase().test()

        val matrixMovie = UserMovie(
            Movie(
                20,
                "The Matrix",
                "overview",
                LocalDate(2020, 1, 20),
                null
            ), false
        )
        val darkKnightMovie = UserMovie(
            Movie(
                30,
                "The Dark Knight",
                "overview",
                LocalDate(2020, 1, 10),
                null
            ), true
        )

        getAllUserMoviesSubject.onNext(listOf(matrixMovie, darkKnightMovie))

        observer.assertValueCount(1)
        observer.assertValue(
            listOf(matrixMovie).sortedBy { it.movie.title }
        )
        observer.dispose()
    }
}