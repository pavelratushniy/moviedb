package com.example.moviedb.domain.interactor

import com.example.moviedb.core.vo.FavouriteMovie
import com.example.moviedb.core.vo.Movie
import com.example.moviedb.data.repo.favouritemovies.FavouriteMoviesRepo
import io.reactivex.Completable
import io.reactivex.Scheduler
import org.joda.time.DateTimeUtils

class AddMovieToFavouritesUseCaseImpl(
    private val favouriteMoviesRepo: FavouriteMoviesRepo,
    private val ioScheduler: Scheduler
) : AddMovieToFavouritesUseCase {

    override operator fun invoke(movie: Movie): Completable {
        return favouriteMoviesRepo.save(
            FavouriteMovie(
                DateTimeUtils.currentTimeMillis(),
                movie.movieId
            )
        ).subscribeOn(ioScheduler)
    }
}

interface AddMovieToFavouritesUseCase {

    operator fun invoke(movie: Movie): Completable
}