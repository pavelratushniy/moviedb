package com.example.moviedb.domain.di

import com.example.moviedb.data.repo.favouritemovies.FavouriteMoviesRepo
import com.example.moviedb.data.repo.movies.MoviesRepo
import com.example.moviedb.domain.interactor.*
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Singleton

@Module
class DomainModule {

    @Provides
    @Singleton
    fun fetchOngoingMoviesForTwoWeeksUseCase(
        moviesRepo: MoviesRepo,
        ioScheduler: Scheduler
    ): FetchOngoingMoviesForTwoWeeksUseCase =
        FetchOngoingMoviesForTwoWeeksUseCaseImpl(moviesRepo, ioScheduler)

    @Provides
    @Singleton
    fun getAllUserMoviesUseCase(
        moviesRepo: MoviesRepo,
        favouriteMoviesRepo: FavouriteMoviesRepo,
        ioScheduler: Scheduler
    ): GetAllUserMoviesUseCase =
        GetAllUserMoviesUseCaseImpl(moviesRepo, favouriteMoviesRepo, ioScheduler)

    @Provides
    @Singleton
    fun getOngoingUserMoviesForTwoWeeksUseCase(
        getAllUserMoviesUseCase: GetAllUserMoviesUseCase,
        ioScheduler: Scheduler
    ): GetOngoingUserMoviesForTwoWeeksUseCase =
        GetOngoingUserMoviesForTwoWeeksUseCaseImpl(getAllUserMoviesUseCase, ioScheduler)

    @Provides
    @Singleton
    fun getFavouriteUserMoviesUseCase(
        moviesRepo: MoviesRepo,
        favouriteMoviesRepo: FavouriteMoviesRepo,
        ioScheduler: Scheduler
    ): GetFavouriteUserMoviesUseCase =
        GetFavouriteUserMoviesUseCaseImpl(moviesRepo, favouriteMoviesRepo, ioScheduler)

    @Provides
    @Singleton
    fun toggleFavouriteUseCase(
        addMovieToFavouritesUseCase: AddMovieToFavouritesUseCase,
        removeMovieFromFavouritesUseCase: RemoveMovieFromFavouritesUseCase,
        favouriteMoviesRepo: FavouriteMoviesRepo,
        ioScheduler: Scheduler
    ): ToggleFavouriteUseCase = ToggleFavouriteUseCaseImpl(
        addMovieToFavouritesUseCase,
        removeMovieFromFavouritesUseCase,
        favouriteMoviesRepo,
        ioScheduler
    )

    @Provides
    @Singleton
    fun addMovieToFavouritesUseCase(
        favouriteMoviesRepo: FavouriteMoviesRepo,
        ioScheduler: Scheduler
    ): AddMovieToFavouritesUseCase = AddMovieToFavouritesUseCaseImpl(favouriteMoviesRepo, ioScheduler)

    @Provides
    @Singleton
    fun removeMovieFromFavouritesUseCase(
        favouriteMoviesRepo: FavouriteMoviesRepo,
        ioScheduler: Scheduler
    ): RemoveMovieFromFavouritesUseCase =
        RemoveMovieFromFavouritesUseCaseImpl(favouriteMoviesRepo, ioScheduler)
}