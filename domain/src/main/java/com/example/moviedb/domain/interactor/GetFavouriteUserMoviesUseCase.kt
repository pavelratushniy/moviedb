package com.example.moviedb.domain.interactor

import com.example.moviedb.core.vo.FavouriteMovie
import com.example.moviedb.core.vo.Movie
import com.example.moviedb.core.vo.UserMovie
import com.example.moviedb.data.repo.favouritemovies.FavouriteMoviesRepo
import com.example.moviedb.data.repo.movies.MoviesRepo
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.rxkotlin.Observables

class GetFavouriteUserMoviesUseCaseImpl(
    private val moviesRepo: MoviesRepo,
    private val favouriteMoviesRepo: FavouriteMoviesRepo,
    private val ioScheduler: Scheduler
) : GetFavouriteUserMoviesUseCase {

    override operator fun invoke(): Observable<List<UserMovie>> {
        return Observables.combineLatest(
            moviesRepo.getAll(),
            favouriteMoviesRepo.getAll()
        ) { all: List<Movie>, favourites: List<FavouriteMovie> ->
            val allMoviesMap = all.associateBy { it.movieId }
            favourites.sortedByDescending { it.timestamp }
                .mapNotNull { allMoviesMap[it.movieId] }
                .map { UserMovie(it, true) }
        }.subscribeOn(ioScheduler)
    }
}

interface GetFavouriteUserMoviesUseCase {

    operator fun invoke(): Observable<List<UserMovie>>
}