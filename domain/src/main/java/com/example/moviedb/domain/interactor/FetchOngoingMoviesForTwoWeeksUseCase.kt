package com.example.moviedb.domain.interactor

import com.example.moviedb.core.vo.Movie
import com.example.moviedb.data.repo.movies.MoviesRepo
import io.reactivex.Scheduler
import io.reactivex.Single
import org.joda.time.LocalDate

class FetchOngoingMoviesForTwoWeeksUseCaseImpl(
    private val moviesRepo: MoviesRepo,
    private val ioScheduler: Scheduler
) : FetchOngoingMoviesForTwoWeeksUseCase {

    override operator fun invoke(): Single<List<Movie>> {
        return LocalDate.now().let { now ->
            moviesRepo.fetch(now.minusWeeks(2), now)
        }.subscribeOn(ioScheduler)
    }
}

interface FetchOngoingMoviesForTwoWeeksUseCase {

    operator fun invoke(): Single<List<Movie>>
}