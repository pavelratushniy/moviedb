package com.example.moviedb.domain.interactor

import com.example.moviedb.core.vo.UserMovie
import io.reactivex.Observable
import io.reactivex.Scheduler
import org.joda.time.LocalDate

class GetOngoingUserMoviesForTwoWeeksUseCaseImpl(
    private val getAllUserMoviesUseCase: GetAllUserMoviesUseCase,
    private val ioScheduler: Scheduler
) : GetOngoingUserMoviesForTwoWeeksUseCase {

    override operator fun invoke(): Observable<List<UserMovie>> {
        return getAllUserMoviesUseCase()
            .map { userMovies ->
                val to = LocalDate.now()
                val from = to.minusWeeks(2)
                userMovies.filter {
                    it.movie.releaseDate.isAfter(from) && it.movie.releaseDate.isBefore(
                        to
                    )
                }
                    .sortedBy { it.movie.title }
            }.subscribeOn(ioScheduler)
    }
}

interface GetOngoingUserMoviesForTwoWeeksUseCase {

    operator fun invoke(): Observable<List<UserMovie>>
}