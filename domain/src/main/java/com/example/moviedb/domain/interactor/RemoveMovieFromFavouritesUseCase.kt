package com.example.moviedb.domain.interactor

import com.example.moviedb.core.vo.Movie
import com.example.moviedb.data.repo.favouritemovies.FavouriteMoviesRepo
import io.reactivex.Completable
import io.reactivex.Scheduler

class RemoveMovieFromFavouritesUseCaseImpl(
    private val favouriteMoviesRepo: FavouriteMoviesRepo,
    private val ioScheduler: Scheduler
) : RemoveMovieFromFavouritesUseCase {

    override operator fun invoke(movie: Movie): Completable {
        return favouriteMoviesRepo.deleteByMovieId(movie.movieId)
            .subscribeOn(ioScheduler)
    }
}

interface RemoveMovieFromFavouritesUseCase {

    operator fun invoke(movie: Movie): Completable
}