package com.example.moviedb.domain.interactor

import com.example.moviedb.core.vo.UserMovie
import com.example.moviedb.data.repo.favouritemovies.FavouriteMoviesRepo
import com.example.moviedb.data.repo.movies.MoviesRepo
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.rxkotlin.Observables
import java.util.*

class GetAllUserMoviesUseCaseImpl(
    private val moviesRepo: MoviesRepo,
    private val favouriteMoviesRepo: FavouriteMoviesRepo,
    private val ioScheduler: Scheduler
) : GetAllUserMoviesUseCase {

    override operator fun invoke(): Observable<List<UserMovie>> {
        return Observables.combineLatest(
            moviesRepo.getAll(),
            favouriteMoviesRepo.getAll().map {
                HashSet<Int>().apply {
                    it.mapTo(this) { it.movieId }
                }
            }
        ) { all, favourites ->
            all.map { UserMovie(it, favourites.contains(it.movieId)) }
        }.subscribeOn(ioScheduler)
    }
}

interface GetAllUserMoviesUseCase {

    operator fun invoke(): Observable<List<UserMovie>>
}