package com.example.moviedb.domain.interactor

import com.example.moviedb.core.vo.Movie
import com.example.moviedb.data.repo.favouritemovies.FavouriteMoviesRepo
import io.reactivex.Completable
import io.reactivex.Scheduler

class ToggleFavouriteUseCaseImpl(
    private val addMovieToFavouritesUseCase: AddMovieToFavouritesUseCase,
    private val removeMovieFromFavouritesUseCase: RemoveMovieFromFavouritesUseCase,
    private val favouriteMoviesRepo: FavouriteMoviesRepo,
    private val ioScheduler: Scheduler
) : ToggleFavouriteUseCase {

    override operator fun invoke(movie: Movie): Completable {
        return favouriteMoviesRepo.getByMovieId(movie.movieId)
            .isEmpty
            .flatMapCompletable {
                if (it) addMovieToFavouritesUseCase(movie) else removeMovieFromFavouritesUseCase(movie)
            }.subscribeOn(ioScheduler)
    }
}

interface ToggleFavouriteUseCase {

    operator fun invoke(movie: Movie): Completable
}