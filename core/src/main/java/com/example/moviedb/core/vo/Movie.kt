package com.example.moviedb.core.vo

import org.joda.time.LocalDate

data class Movie(
    val movieId: Int,
    val title: String,
    val overview: String,
    val releaseDate: LocalDate,
    val posterPath: String?
)