package com.example.moviedb.core.utils

import timber.log.Timber

fun Any.logDebug(tag: String? = null, message: String, throwable: Throwable? = null) {
    Timber.tag(tag ?: this.javaClass.simpleName).d(throwable, message)
}

fun Any.logVerbose(tag: String? = null, message: String, throwable: Throwable? = null) {
    Timber.tag(tag ?: this.javaClass.simpleName).v(throwable, message)
}

fun Any.logError(tag: String? = null, message: String, throwable: Throwable? = null) {
    Timber.tag(tag ?: this.javaClass.simpleName).e(throwable, message)
}

fun Any.logWarn(tag: String? = null, message: String, throwable: Throwable? = null) {
    Timber.tag(tag ?: this.javaClass.simpleName).w(throwable, message)
}

fun Any.logInfo(tag: String? = null, message: String, throwable: Throwable? = null) {
    Timber.tag(tag ?: this.javaClass.simpleName).i(throwable, message)
}
