package com.example.moviedb.core.vo

data class FavouriteMovie(
    val timestamp: Long,
    val movieId: Int
)