package com.example.moviedb.core.vo

data class UserMovie(
    val movie: Movie,
    val favourite: Boolean
)