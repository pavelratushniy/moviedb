package com.example.moviedb.data.local.movies

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import org.joda.time.LocalDate

@Entity(tableName = "movies")
internal data class RoomMovie(
    @PrimaryKey
    @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "releaseDate") val releaseDate: LocalDate,
    @ColumnInfo(name = "posterPath") val posterPath: String?,
    @ColumnInfo(name = "overview") val overview: String
) {

    class Converter {

        @TypeConverter
        internal fun fromString(value: String): LocalDate {
            return LocalDate.parse(value)
        }

        @TypeConverter
        internal fun fromLocalDate(localDate: LocalDate): String {
            return localDate.toString()
        }
    }
}