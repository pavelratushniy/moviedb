package com.example.moviedb.data.remote.movies

import com.example.moviedb.core.vo.Movie
import io.reactivex.Single
import org.joda.time.LocalDate

interface RemoteMoviesDataSource {

    fun getForTimeframe(from: LocalDate, to: LocalDate): Single<List<Movie>>
}