package com.example.moviedb.data.local.favouritemovies

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable

@Dao
internal abstract class FavouriteMoviesDao {

    @Query("SELECT * FROM favouriteMovies")
    abstract fun getAll(): Observable<List<RoomFavouriteMovie>>

    @Insert
    abstract fun save(roomFavouriteMovie: RoomFavouriteMovie): Completable

    @Query("SELECT * FROM favouriteMovies WHERE movieId = :movieId")
    abstract fun getByMovieId(movieId: Int): Maybe<RoomFavouriteMovie>

    @Query("DELETE FROM favouriteMovies WHERE movieId = :movieId")
    abstract fun deleteByMovieId(movieId: Int): Completable
}