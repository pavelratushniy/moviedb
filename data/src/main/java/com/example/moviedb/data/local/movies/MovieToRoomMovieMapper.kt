package com.example.moviedb.data.local.movies

import com.example.moviedb.core.vo.Movie
import com.example.moviedb.data.Mapper

internal class MovieToRoomMovieMapper : Mapper<Movie, RoomMovie> {

    override fun map(item: Movie): RoomMovie {
        return RoomMovie(
            id = item.movieId,
            title = item.title,
            releaseDate = item.releaseDate,
            posterPath = item.posterPath,
            overview = item.overview
        )
    }
}