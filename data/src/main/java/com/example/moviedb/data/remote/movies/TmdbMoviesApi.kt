package com.example.moviedb.data.remote.movies

import com.example.moviedb.data.stableJson
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import io.reactivex.Single
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import org.joda.time.DateTime
import org.joda.time.LocalDate
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.lang.reflect.Type

internal interface TmdbMoviesApi {

    @GET("movie")
    fun discover(
        @Query("api_key") apiKey: String,
        @Query("primary_release_date.gte") from: LocalDate,
        @Query("primary_release_date.lte") to: LocalDate
    ): Single<TmdbDiscoverResults>

    companion object {

        private const val BASE_URL = "https://api.themoviedb.org/3/discover/"

        fun create(
            client: OkHttpClient = OkHttpClient()
        ): TmdbMoviesApi {
            DateTime.now()
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(
                    stableJson.asConverterFactory(
                        "application/json".toMediaType()
                    )
                )
                .addConverterFactory(TmdbConverterFactory())
                .client(client)
                .build()
                .create(TmdbMoviesApi::class.java)
        }
    }
}

private class TmdbConverterFactory : Converter.Factory() {

    private val timestampConverterFactory = TimestampConverterFactory()

    override fun stringConverter(
        type: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): Converter<*, String>? {
        if (type == LocalDate::class) {
            return timestampConverterFactory
        }
        return super.stringConverter(type, annotations, retrofit)
    }
}

private class TimestampConverterFactory : Converter<LocalDate, String> {

    override fun convert(value: LocalDate): String? {
        return value.toString("yyyy-MM-dd")
    }
}