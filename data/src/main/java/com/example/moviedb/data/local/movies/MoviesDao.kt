package com.example.moviedb.data.local.movies

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Observable

@Dao
internal abstract class MoviesDao {

    @Query("SELECT * FROM movies")
    abstract fun getAll(): Observable<List<RoomMovie>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(movie: RoomMovie): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(movies: List<RoomMovie>): Completable

    @Delete
    abstract fun delete(movie: RoomMovie): Completable
}