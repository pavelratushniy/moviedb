package com.example.moviedb.data.remote.movies

import com.example.moviedb.core.vo.Movie
import com.example.moviedb.data.Mapper
import org.joda.time.LocalDate

private const val TMDB_IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w185"

internal class TmdbMovieToMovieMapper : Mapper<TmdbMovie, Movie> {

    override fun map(item: TmdbMovie): Movie {
        return Movie(
            movieId = item.id,
            title = item.title,
            releaseDate = LocalDate.parse(item.releaseDate),
            posterPath = if (item.posterPath == null) null else "$TMDB_IMAGE_BASE_URL/${item.posterPath}",
            overview = item.overview
        )
    }
}