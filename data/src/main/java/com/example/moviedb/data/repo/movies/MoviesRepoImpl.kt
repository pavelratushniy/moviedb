package com.example.moviedb.data.repo.movies

import com.example.moviedb.core.vo.Movie
import com.example.moviedb.data.local.movies.LocalMoviesDataSource
import com.example.moviedb.data.remote.movies.RemoteMoviesDataSource
import io.reactivex.Observable
import io.reactivex.Single
import org.joda.time.LocalDate

class MoviesRepoImpl(
    private val localMoviesDataSource: LocalMoviesDataSource,
    private val remoteMoviesDataSource: RemoteMoviesDataSource
) : MoviesRepo {

    override fun fetch(from: LocalDate, to: LocalDate): Single<List<Movie>> {
        return remoteMoviesDataSource.getForTimeframe(from, to)
            .flatMap { localMoviesDataSource.save(it).andThen(Single.just(it)) }
    }

    override fun getAll(): Observable<List<Movie>> {
        return localMoviesDataSource.getAll()
    }
}