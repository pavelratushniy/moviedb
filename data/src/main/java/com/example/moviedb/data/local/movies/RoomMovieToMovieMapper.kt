package com.example.moviedb.data.local.movies

import com.example.moviedb.core.vo.Movie
import com.example.moviedb.data.Mapper

internal class RoomMovieToMovieMapper : Mapper<RoomMovie, Movie> {

    override fun map(item: RoomMovie): Movie {
        return Movie(
            movieId = item.id,
            title = item.title,
            releaseDate = item.releaseDate,
            posterPath = item.posterPath,
            overview = item.overview
        )
    }
}