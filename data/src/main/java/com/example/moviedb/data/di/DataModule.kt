package com.example.moviedb.data.di

import android.app.Application
import androidx.room.Room
import com.example.moviedb.data.BuildConfig
import com.example.moviedb.data.local.AppDatabase
import com.example.moviedb.data.local.favouritemovies.FavouriteMovieToRoomFavouriteMovieMapper
import com.example.moviedb.data.local.favouritemovies.LocalFavouriteMoviesDataSource
import com.example.moviedb.data.local.favouritemovies.RoomFavouriteMovieToFavouriteMovieMapper
import com.example.moviedb.data.local.favouritemovies.RoomLocalFavouriteMoviesDataSource
import com.example.moviedb.data.local.movies.LocalMoviesDataSource
import com.example.moviedb.data.local.movies.MovieToRoomMovieMapper
import com.example.moviedb.data.local.movies.RoomLocalMoviesDataSource
import com.example.moviedb.data.local.movies.RoomMovieToMovieMapper
import com.example.moviedb.data.remote.movies.RemoteMoviesDataSource
import com.example.moviedb.data.remote.movies.TmdbMovieToMovieMapper
import com.example.moviedb.data.remote.movies.TmdbMoviesApi
import com.example.moviedb.data.remote.movies.TmdbRemoteMoviesDataSource
import com.example.moviedb.data.repo.favouritemovies.FavouriteMoviesRepo
import com.example.moviedb.data.repo.favouritemovies.FavouriteMoviesRepoImpl
import com.example.moviedb.data.repo.movies.MoviesRepo
import com.example.moviedb.data.repo.movies.MoviesRepoImpl
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Singleton

@Module
class DataModule(private val app: Application) {

    private val db = Room
        .databaseBuilder(app, AppDatabase::class.java, "movies_data.db")
        .build()

    @Provides
    @Singleton
    fun app() = app

    @Provides
    @Singleton
    fun ioScheduler(): Scheduler = Schedulers.io()

    @Provides
    @Singleton
    fun remoteMoviesDateSource(): RemoteMoviesDataSource = TmdbRemoteMoviesDataSource(
        BuildConfig.TMDB_API_KEY,
        TmdbMoviesApi.create(createTmdbApiClient()),
        TmdbMovieToMovieMapper()
    )

    @Provides
    @Singleton
    fun localMoviesDataSource(): LocalMoviesDataSource = RoomLocalMoviesDataSource(
        db.moviesDao(),
        RoomMovieToMovieMapper(),
        MovieToRoomMovieMapper()
    )

    @Provides
    @Singleton
    fun moviesRepo(
        remoteMoviesDataSource: RemoteMoviesDataSource,
        localMoviesDataSource: LocalMoviesDataSource
    ): MoviesRepo = MoviesRepoImpl(localMoviesDataSource, remoteMoviesDataSource)

    @Provides
    @Singleton
    fun localFavouriteMoviesDataSource(): LocalFavouriteMoviesDataSource {
        return RoomLocalFavouriteMoviesDataSource(
            db.favouriteMoviesDao(),
            FavouriteMovieToRoomFavouriteMovieMapper(),
            RoomFavouriteMovieToFavouriteMovieMapper()
        )
    }

    @Provides
    @Singleton
    fun favouriteMoviesRepo(
        localFavouriteMoviesDataSource: LocalFavouriteMoviesDataSource
    ): FavouriteMoviesRepo = FavouriteMoviesRepoImpl(localFavouriteMoviesDataSource)

    private fun createTmdbApiClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = if (BuildConfig.DEBUG) {
                    HttpLoggingInterceptor.Level.BODY
                } else {
                    HttpLoggingInterceptor.Level.NONE
                }
            }).build()
    }
}