package com.example.moviedb.data.local.movies

import com.example.moviedb.core.vo.Movie
import io.reactivex.Completable
import io.reactivex.Observable

interface LocalMoviesDataSource {

    fun getAll(): Observable<List<Movie>>

    fun save(movies: List<Movie>): Completable
}