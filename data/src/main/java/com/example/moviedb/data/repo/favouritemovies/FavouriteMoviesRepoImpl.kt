package com.example.moviedb.data.repo.favouritemovies

import com.example.moviedb.core.vo.FavouriteMovie
import com.example.moviedb.data.local.favouritemovies.LocalFavouriteMoviesDataSource
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable

class FavouriteMoviesRepoImpl(
    private val favouriteMoviesDataSource: LocalFavouriteMoviesDataSource
) : FavouriteMoviesRepo {

    override fun getAll(): Observable<List<FavouriteMovie>> {
        return favouriteMoviesDataSource.getAll()
    }

    override fun save(favouriteMovie: FavouriteMovie): Completable {
        return favouriteMoviesDataSource.save(favouriteMovie)
    }

    override fun getByMovieId(movieId: Int): Maybe<FavouriteMovie> {
        return favouriteMoviesDataSource.getByMovieId(movieId)
    }

    override fun deleteByMovieId(movieId: Int): Completable {
        return favouriteMoviesDataSource.deleteByMovieId(movieId)
    }
}