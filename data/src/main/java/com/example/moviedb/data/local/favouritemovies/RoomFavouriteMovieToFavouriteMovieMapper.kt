package com.example.moviedb.data.local.favouritemovies

import com.example.moviedb.core.vo.FavouriteMovie
import com.example.moviedb.data.Mapper

internal class RoomFavouriteMovieToFavouriteMovieMapper :
    Mapper<RoomFavouriteMovie, FavouriteMovie> {

    override fun map(item: RoomFavouriteMovie): FavouriteMovie {
        return FavouriteMovie(
            timestamp = item.timestamp,
            movieId = item.movieId
        )
    }
}