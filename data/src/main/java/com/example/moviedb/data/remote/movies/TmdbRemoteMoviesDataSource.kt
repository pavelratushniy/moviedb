package com.example.moviedb.data.remote.movies

import com.example.moviedb.core.vo.Movie
import com.example.moviedb.data.Mapper
import io.reactivex.Single
import org.joda.time.LocalDate

internal class TmdbRemoteMoviesDataSource(
    private val apiKey: String,
    private val tmdbMoviesApi: TmdbMoviesApi,
    private val tmdbMovieToMovieMapper: Mapper<TmdbMovie, Movie>
) : RemoteMoviesDataSource {

    override fun getForTimeframe(from: LocalDate, to: LocalDate): Single<List<Movie>> {
        return tmdbMoviesApi.discover(apiKey, from, to)
            .map { it.results }
            .map { tmdbMovieToMovieMapper.map(it) }
    }
}