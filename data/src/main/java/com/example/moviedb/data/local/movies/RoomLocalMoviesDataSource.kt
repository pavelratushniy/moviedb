package com.example.moviedb.data.local.movies

import com.example.moviedb.core.vo.Movie
import com.example.moviedb.data.Mapper
import io.reactivex.Completable
import io.reactivex.Observable

internal class RoomLocalMoviesDataSource(
    private val moviesDao: MoviesDao,
    private val roomMovieToMovieMapper: Mapper<RoomMovie, Movie>,
    private val movieToRoomMovieMapper: Mapper<Movie, RoomMovie>
) : LocalMoviesDataSource {

    override fun getAll(): Observable<List<Movie>> {
        return moviesDao.getAll().map { roomMovies ->
            roomMovieToMovieMapper.map(roomMovies)
        }
    }

    override fun save(movies: List<Movie>): Completable {
        return moviesDao.insertAll(movieToRoomMovieMapper.map(movies))
    }
}