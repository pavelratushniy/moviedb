package com.example.moviedb.data.remote.movies

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class TmdbDiscoverResults(
    @SerialName("page") val page: Int,
    @SerialName("results") val results: List<TmdbMovie>,
    @SerialName("total_results") val totalResults: Int,
    @SerialName("total_pages") val totalPages: Int
)

@Serializable
internal data class TmdbMovie(
    @SerialName("poster_path") val posterPath: String? = null,
    val adult: Boolean,
    val overview: String,
    @SerialName("release_date") val releaseDate: String,
    @SerialName("genre_ids") val genreIds: List<Int>,
    val id: Int,
    @SerialName("original_title") val originalTitle: String,
    @SerialName("original_language") val originalLanguage: String,
    val title: String,
    @SerialName("backdrop_path") val backdropPath: String? = null,
    val popularity: Double,
    @SerialName("vote_count") val voteCount: Int,
    val video: Boolean,
    @SerialName("vote_average") val voteAverage: Double
)

