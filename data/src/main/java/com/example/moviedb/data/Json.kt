package com.example.moviedb.data

import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration

val stableJson = Json(JsonConfiguration.Stable)
