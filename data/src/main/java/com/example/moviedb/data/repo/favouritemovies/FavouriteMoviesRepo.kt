package com.example.moviedb.data.repo.favouritemovies

import com.example.moviedb.core.vo.FavouriteMovie
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable

interface FavouriteMoviesRepo {

    fun getAll(): Observable<List<FavouriteMovie>>

    fun save(favouriteMovie: FavouriteMovie): Completable

    fun getByMovieId(movieId: Int): Maybe<FavouriteMovie>

    fun deleteByMovieId(movieId: Int): Completable
}