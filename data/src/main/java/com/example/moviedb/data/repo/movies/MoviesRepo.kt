package com.example.moviedb.data.repo.movies

import com.example.moviedb.core.vo.Movie
import io.reactivex.Observable
import io.reactivex.Single
import org.joda.time.LocalDate

interface MoviesRepo {

    fun fetch(from: LocalDate, to: LocalDate): Single<List<Movie>>

    fun getAll(): Observable<List<Movie>>
}