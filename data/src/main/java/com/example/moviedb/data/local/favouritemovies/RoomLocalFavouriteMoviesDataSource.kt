package com.example.moviedb.data.local.favouritemovies

import com.example.moviedb.core.vo.FavouriteMovie
import com.example.moviedb.data.Mapper
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable

internal class RoomLocalFavouriteMoviesDataSource(
    private val favouriteMoviesDao: FavouriteMoviesDao,
    private val favouriteMovieToRoomFavouriteMovieMapper: Mapper<FavouriteMovie, RoomFavouriteMovie>,
    private val roomFavouriteMovieToFavouriteMovieMapper: Mapper<RoomFavouriteMovie, FavouriteMovie>
) : LocalFavouriteMoviesDataSource {

    override fun getAll(): Observable<List<FavouriteMovie>> {
        return favouriteMoviesDao.getAll()
            .map { roomFavouriteMovies ->
                roomFavouriteMovieToFavouriteMovieMapper.map(
                    roomFavouriteMovies
                )
            }
    }

    override fun save(favouriteMovie: FavouriteMovie): Completable {
        return favouriteMoviesDao.save(favouriteMovieToRoomFavouriteMovieMapper.map(favouriteMovie))
    }

    override fun getByMovieId(movieId: Int): Maybe<FavouriteMovie> {
        return favouriteMoviesDao.getByMovieId(movieId)
            .map { roomFavouriteMovieToFavouriteMovieMapper.map(it) }
    }

    override fun deleteByMovieId(movieId: Int): Completable {
        return favouriteMoviesDao.deleteByMovieId(movieId)
    }
}