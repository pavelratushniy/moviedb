package com.example.moviedb.data.local.favouritemovies

import com.example.moviedb.core.vo.FavouriteMovie
import com.example.moviedb.data.Mapper

internal class FavouriteMovieToRoomFavouriteMovieMapper : Mapper<FavouriteMovie, RoomFavouriteMovie> {

    override fun map(item: FavouriteMovie): RoomFavouriteMovie {
        return RoomFavouriteMovie(
            timestamp = item.timestamp,
            movieId = item.movieId
        )
    }
}