package com.example.moviedb.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.moviedb.data.local.favouritemovies.FavouriteMoviesDao
import com.example.moviedb.data.local.favouritemovies.RoomFavouriteMovie
import com.example.moviedb.data.local.movies.MoviesDao
import com.example.moviedb.data.local.movies.RoomMovie

@Database(
    entities = [
        RoomMovie::class,
        RoomFavouriteMovie::class
    ],
    version = 1,
    exportSchema = false
)
@TypeConverters(RoomMovie.Converter::class)
internal abstract class AppDatabase : RoomDatabase() {
    abstract fun moviesDao(): MoviesDao
    abstract fun favouriteMoviesDao(): FavouriteMoviesDao
}
