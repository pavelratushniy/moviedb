package com.example.moviedb.data.local.favouritemovies

import android.app.Application
import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.moviedb.data.local.AppDatabase
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class FavouriteMoviesDaoInstrumentedTest {

    private lateinit var favouriteMoviesDao: FavouriteMoviesDao

    @Before
    fun setUp() {
        val app =
            InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as Application
        val appDatabase = Room.inMemoryDatabaseBuilder(app, AppDatabase::class.java).build()

        favouriteMoviesDao = appDatabase.favouriteMoviesDao()
    }

    @Test
    fun testInsertingFavouriteMovie() {
        val roomFavouriteMovie = RoomFavouriteMovie(
            20,
            1,
            30
        )

        val saveObserver = favouriteMoviesDao.save(roomFavouriteMovie).test()

        saveObserver.assertComplete()
        saveObserver.dispose()

        val loadObserver = favouriteMoviesDao.getAll().test()

        loadObserver.awaitCount(1)
        loadObserver.assertValues(listOf(roomFavouriteMovie))
        loadObserver.dispose()
    }

    @Test
    fun testGettingFavouriteMovieByMovieId() {
        val roomFavouriteMovie = RoomFavouriteMovie(
            20,
            1,
            30
        )

        val saveObserver = favouriteMoviesDao.save(roomFavouriteMovie).test()

        saveObserver.assertComplete()
        saveObserver.dispose()

        val loadObserver = favouriteMoviesDao.getByMovieId(roomFavouriteMovie.movieId).test()

        loadObserver.awaitCount(1)
        loadObserver.assertValue(roomFavouriteMovie)
        loadObserver.dispose()
    }

    @Test
    fun testGettingFavouriteMovieByMovieIdNoResults() {
        val loadObserver = favouriteMoviesDao.getByMovieId(30).test()

        loadObserver.assertNoValues()
        loadObserver.assertComplete()
        loadObserver.dispose()
    }

    @Test
    fun testDeletingByMovieId() {
        val roomFavouriteMovie = RoomFavouriteMovie(
            20,
            1,
            30
        )

        val saveObserver = favouriteMoviesDao.save(roomFavouriteMovie).test()

        saveObserver.assertComplete()
        saveObserver.dispose()

        val loadObserver = favouriteMoviesDao.deleteByMovieId(roomFavouriteMovie.movieId).test()

        loadObserver.assertComplete()
        loadObserver.dispose()
    }
}