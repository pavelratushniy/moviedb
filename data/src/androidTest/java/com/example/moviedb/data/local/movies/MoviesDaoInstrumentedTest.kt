package com.example.moviedb.data.local.movies

import android.app.Application
import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.moviedb.data.local.AppDatabase
import org.joda.time.LocalDate
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MoviesDaoInstrumentedTest {

    private lateinit var moviesDao: MoviesDao

    @Before
    fun setUp() {
        val app =
            InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as Application
        val appDatabase = Room.inMemoryDatabaseBuilder(app, AppDatabase::class.java).build()

        moviesDao = appDatabase.moviesDao()
    }

    @Test
    fun testInsertingMovie() {
        val roomMovie = RoomMovie(
            20,
            "The Matrix",
            LocalDate(2020, 1, 1),
            "posterPath",
            "overview"
        )

        val saveObserver = moviesDao.insert(roomMovie).test()

        saveObserver.assertComplete()
        saveObserver.dispose()

        val loadObserver = moviesDao.getAll().test()

        loadObserver.awaitCount(1)
        loadObserver.assertValues(listOf(roomMovie))
        loadObserver.dispose()
    }

    @Test
    fun testInsertingMovies() {
        val roomMovies = listOf(
            RoomMovie(
                20,
                "The Matrix",
                LocalDate(2020, 1, 1),
                "posterPath",
                "overview"
            ),
            RoomMovie(
                30,
                "The Dark Knight",
                LocalDate(2020, 1, 1),
                "posterPath",
                "overview"
            )
        )

        val saveObserver = moviesDao.insertAll(roomMovies).test()

        saveObserver.assertComplete()
        saveObserver.dispose()

        val loadObserver = moviesDao.getAll().test()

        loadObserver.awaitCount(1)
        loadObserver.assertValues(roomMovies)
        loadObserver.dispose()
    }

    @Test
    fun testDeletingMovie() {
        val roomMovie = RoomMovie(
            20,
            "The Matrix",
            LocalDate(2020, 1, 1),
            "posterPath",
            "overview"
        )

        val saveObserver = moviesDao.insert(roomMovie).test().await()

        saveObserver.assertComplete()
        saveObserver.dispose()

        val deleteObserver = moviesDao.delete(roomMovie).test().await()

        deleteObserver.assertComplete()
        deleteObserver.dispose()

        val loadObserver = moviesDao.getAll().test()

        loadObserver.awaitCount(1)
        loadObserver.assertValues(
            emptyList()
        )
        loadObserver.dispose()
    }

    @Test
    fun testUpdatingMovie() {
        val roomMovie = RoomMovie(
            20,
            "The Matrix",
            LocalDate(2020, 1, 1),
            "posterPath",
            "overview"
        )

        val saveObserver = moviesDao.insert(roomMovie).test().await()

        saveObserver.assertComplete()
        saveObserver.dispose()

        val roomMovieUpdated = roomMovie.copy(overview = "updated overview")
        val updateObserver = moviesDao.insert(roomMovieUpdated).test().await()

        updateObserver.assertComplete()
        updateObserver.dispose()

        val loadObserver = moviesDao.getAll().test()

        loadObserver.awaitCount(1)
        loadObserver.assertValues(
            listOf(roomMovieUpdated)
        )
        loadObserver.dispose()
    }
}