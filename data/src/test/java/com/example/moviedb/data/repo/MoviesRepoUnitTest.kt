package com.example.moviedb.data.repo

import com.example.moviedb.core.vo.Movie
import com.example.moviedb.data.local.movies.LocalMoviesDataSource
import com.example.moviedb.data.remote.movies.RemoteMoviesDataSource
import com.example.moviedb.data.repo.movies.MoviesRepo
import com.example.moviedb.data.repo.movies.MoviesRepoImpl
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.SingleSubject
import org.joda.time.LocalDate
import org.junit.Before
import org.junit.Test

class MoviesRepoUnitTest {

    private lateinit var localMoviesDataSource: LocalMoviesDataSource
    private lateinit var remoteMoviesDataSource: RemoteMoviesDataSource

    private lateinit var moviesRepo: MoviesRepo

    @Before
    fun init() {
        localMoviesDataSource = mock()
        remoteMoviesDataSource = mock()

        moviesRepo = MoviesRepoImpl(
            localMoviesDataSource,
            remoteMoviesDataSource
        )
    }

    @Test
    fun `test getting all movies`() {
        val getAllMoviesSubject = BehaviorSubject.create<List<Movie>>()
        whenever(localMoviesDataSource.getAll()) doReturn getAllMoviesSubject

        val loadObserver = moviesRepo.getAll().test()

        val movies = listOf(
            Movie(
                20,
                "The Matrix",
                "overview",
                LocalDate(2020, 1, 1),
                null
            )
        )

        getAllMoviesSubject.onNext(movies)

        loadObserver.awaitCount(1)
        loadObserver.assertValue(movies)
        loadObserver.dispose()
    }

    @Test
    fun `test fetch`() {
        val getMoviesForTimeframeSubject = SingleSubject.create<List<Movie>>()
        whenever(
            remoteMoviesDataSource.getForTimeframe(
                any(),
                any()
            )
        ) doReturn getMoviesForTimeframeSubject

        val saveMoviesSubject = CompletableSubject.create()
        whenever(localMoviesDataSource.save(any())) doReturn saveMoviesSubject

        val fetchObserver = moviesRepo.fetch(
            LocalDate(2020, 1, 10),
            LocalDate(2020, 1, 20)
        ).test()

        val movies = listOf(
            Movie(
                20,
                "The Matrix",
                "overview",
                LocalDate(2020, 1, 1),
                null
            )
        )

        getMoviesForTimeframeSubject.onSuccess(movies)
        saveMoviesSubject.onComplete()

        fetchObserver.assertValue(movies)
        fetchObserver.dispose()
    }
}