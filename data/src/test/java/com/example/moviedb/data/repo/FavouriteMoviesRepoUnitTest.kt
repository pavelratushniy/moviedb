package com.example.moviedb.data.repo

import com.example.moviedb.core.vo.FavouriteMovie
import com.example.moviedb.data.local.favouritemovies.LocalFavouriteMoviesDataSource
import com.example.moviedb.data.repo.favouritemovies.FavouriteMoviesRepo
import com.example.moviedb.data.repo.favouritemovies.FavouriteMoviesRepoImpl
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.MaybeSubject
import org.junit.Before
import org.junit.Test

class FavouriteMoviesRepoUnitTest {

    private lateinit var localFavouriteMoviesDataSource: LocalFavouriteMoviesDataSource

    private lateinit var favouriteMoviesRepo: FavouriteMoviesRepo

    @Before
    fun init() {
        localFavouriteMoviesDataSource = mock()

        favouriteMoviesRepo = FavouriteMoviesRepoImpl(
            localFavouriteMoviesDataSource
        )
    }

    @Test
    fun `test getting all favourite movies`() {
        val getAllFavouriteMoviesSubject = BehaviorSubject.create<List<FavouriteMovie>>()
        whenever(localFavouriteMoviesDataSource.getAll()) doReturn getAllFavouriteMoviesSubject

        val loadObserver = favouriteMoviesRepo.getAll().test()

        val favouriteMovies = listOf(
            FavouriteMovie(1, 20)
        )

        getAllFavouriteMoviesSubject.onNext(favouriteMovies)

        loadObserver.awaitCount(1)
        loadObserver.assertValue(favouriteMovies)
        loadObserver.dispose()
    }

    @Test
    fun `test saving favourite movie`() {
        val saveFavouriteMovieSubject = CompletableSubject.create()
        whenever(localFavouriteMoviesDataSource.save(any())) doReturn saveFavouriteMovieSubject

        val saveObserver = favouriteMoviesRepo.save(FavouriteMovie(1, 20)).test()

        saveFavouriteMovieSubject.onComplete()

        saveObserver.assertComplete()
        saveObserver.dispose()
    }

    @Test
    fun `test getting by movie id`() {
        val getFavouriteMovieByMovieIdSubject = MaybeSubject.create<FavouriteMovie>()
        whenever(localFavouriteMoviesDataSource.getByMovieId(any())) doReturn getFavouriteMovieByMovieIdSubject

        val loadObserver = favouriteMoviesRepo.getByMovieId(20).test()

        val favouriteMovie = FavouriteMovie(1, 20)
        getFavouriteMovieByMovieIdSubject.onSuccess(favouriteMovie)

        loadObserver.assertValue(favouriteMovie)
        loadObserver.dispose()
    }

    @Test
    fun `test deleting by movie id`() {
        val deleteFavouriteMovieSubject = CompletableSubject.create()
        whenever(localFavouriteMoviesDataSource.deleteByMovieId(any())) doReturn deleteFavouriteMovieSubject

        val deleteObserver = favouriteMoviesRepo.deleteByMovieId(20).test()

        deleteFavouriteMovieSubject.onComplete()

        deleteObserver.assertComplete()
        deleteObserver.dispose()
    }
}